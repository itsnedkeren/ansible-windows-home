﻿###### JYLS: kør som admin ######
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }

######: JYLS: force EP til Bypass for processen - scriptet indeholder fejl men kan ignoreres. #####
Set-ExecutionPolicy -ExecutionPolicy Bypass -Scope Process -Force

####### JYLS: variabler til konto ######
Write-Host "Brugernavn:"
$accusername = Read-Host
Write-Host "Password:"
$accpassword = Read-Host -AsSecureString
Write-Host "Beskrivelse af konto:"
$accdesc = Read-Host
Write-Host "Fuldt navn:"
$accfullname = Read-Host

New-LocalUser $accusername -Password $accpassword -FullName $accfullname -Description $accdesc

Add-LocalGroupMember -Group "Administrators" -Member $accusername

###### JYLS: kører psh og .net upgrade ######
Write-Host "Running Upgrade-PowerShell script"
$url = "https://raw.githubusercontent.com/jborean93/ansible-windows/master/scripts/Upgrade-PowerShell.ps1"
$file = "$env:temp\Upgrade-PowerShell.ps1"
###### do NOT use Administrator account - create seperate account
$username = $accusername
$password = $accpassword

(New-Object -TypeName System.Net.WebClient).DownloadFile($url, $file)
Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Force

# Version can be 3.0, 4.0 or 5.1
# JYLS: Version fjernet, vi vil have latest
&$file -Username $username -Password $password -Verbose

###### JYLS: kører wmf3hotfix #####
Write-Host "Running WMF3Hotfix script"
$url = "https://raw.githubusercontent.com/jborean93/ansible-windows/master/scripts/Install-WMF3Hotfix.ps1"
$file = "$env:temp\Install-WMF3Hotfix.ps1"

(New-Object -TypeName System.Net.WebClient).DownloadFile($url, $file)
powershell.exe -ExecutionPolicy ByPass -File $file -Verbose

###### JYLS: kører WinRM setup til Ansible #####
Write-Host "Running WinRM setup script"
$url = "https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1"
$file = "$env:temp\ConfigureRemotingForAnsible.ps1"
(New-Object -TypeName System.Net.WebClient).DownloadFile($url, $file)
powershell.exe -ExecutionPolicy ByPass -File $file

###### JYLS: jeg vil ikke have HTTP listener, så vi sletter######
Get-ChildItem -Path WSMan:\localhost\Listener | Where-Object { $_.Keys -contains "Transport=HTTP" } | Remove-Item -Recurse -Force


######################### END ########################
# This isn't needed but is a good security practice to complete
Set-ExecutionPolicy -ExecutionPolicy Restricted -Force

$reg_winlogon_path = "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Winlogon"
Set-ItemProperty -Path $reg_winlogon_path -Name AutoAdminLogon -Value 0
Remove-ItemProperty -Path $reg_winlogon_path -Name DefaultUserName -ErrorAction SilentlyContinue
Remove-ItemProperty -Path $reg_winlogon_path -Name DefaultPassword -ErrorAction SilentlyContinue
######################################################