# Ansible-windows-home
Enable Remote Windows PC's to respond to Ansible playbooks and requests.
## Getting started
Execute GO.ps1 on the client machine, you wish to remotely manage.
### Prerequisites
The script will handle any updates needed for the client system.
Use NTLM as authentication for secure connection when using Ansible
Use HTTPS for communication when using Ansible
## License
MIT
